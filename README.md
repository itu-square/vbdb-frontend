VBDB frontend
=============

##Production

* http://vbdb.itu.dk/
* http://vbdb.itu.dk/#home
* http://vbdb.itu.dk/#bug
* http://vbdb.itu.dk/#bug/{repoName}/{bugId}
* http://vbdb.itu.dk/#search/{criteria}
* http://vbdb.itu.dk/#contribution

The bugs showcased in this web-app are gathered from the following repository: https://bitbucket.org/modelsteam/vbdb/

##Dependencies

* Bootstrap [http://getbootstrap.com/](http://getbootstrap.com/)
* Backbone.js [http://backbonejs.org/](http://backbonejs.org/)
* Syntax Highlighter [http://balupton.github.io/jquery-syntaxhighlighter/demo/](http://balupton.github.io/jquery-syntaxhighlighter/demo/)
* JS-YAML [http://nodeca.github.io/js-yaml/](http://nodeca.github.io/js-yaml/) * Infinite Scroll [http://www.infinite-scroll.com/](http://www.infinite-scroll.com/)

##FAQ

### Where to modify the HTML structure / static texts ?

The file `index.html` describe the main scaffold of the web application, it also have internal templates under the tag
```
<script type="text/template">
```
Then, each page of the web application have a specific HTML template located under the folder `assets/template`.
It is therefore possible to direclty modify static texts in these files such as the home page header section or the contribution page. For example the text in the navigation bar "BitBucket Linux Bug Tracker" or the website logo title "ITU Linux bug reports" can be change in the file `index.html`.

### How to add a new article on the home page ?

Just add a new HTML page in the folder `data/news/` (using the same template as the others) The file names are used to sort the news articles on the web page, the smaller the number in the file name, the older it will appear. Therefore a new article must have a greater number in its name than the one already in the folder.

### How to change the number of news that are loaded on the home page ?

Each time the user reach the bottom of the home page, a new request is made to the server to load the next articles. To change the number of articles to load at each step, change the value of the constant $NEWS_NUMBER at the line 6 in the file `script/news.php`.

### How to delete an article ?

Find the article to remove into the folder `data/news/` and just delete or move the HTML file outside of the folder.

### Where to modify constants values, path, rules ?

All the constants values are in the file `assets/js/model/Store.js`. Let's review each of the data. All the configuration values used are the keys of the YAML report file. 

TREE is defining the repository information. If at some point, the repo location or branch have to change, this is the variables to update. It is also possible to add new repository to this object.

FOLDERS is an array containing the different folders name for which the web app need to load files from.

TABLE_INFO is used to define which data need to appear in the bug page's  table.

HIDDEN_FIELDS is used to prevent some data from being displayed on the bug page.

PATH, BRANCH and REPOSITORY are just internal constants used to construct dynamic URLs.

RESOURCE_PATH is the URL to the RESTFul API of Bitbucket allowing to access content in the JSON format.

RAW_LINK is the URL to the raw files on Bitbucket.

CWE_LINK is the URL to the Common Weakness Enumeration webpage.

TAXONOMY_PATH is the name of the taxonomy file on the repo.

COMMENTS and COMMENTS_WEBSERVICE are the path to the comments file on the server and the PHP webservice to save comments on this file, respectively.

NEWS and NEWS_WEBSERVICE are the path to the news folder and the PHP webservice to load them, respectively.

CAPTCHA_WEBSERVICE is the path to the captcha webservice generating the challenges.

DISCUSSION_TAB_LABEL is simply the string to display as a panel button for the comment tab.

RESULT_COUNTER_LABEL is the label to the counter label on the search page.

CONTENT_WORDING is the text displayed for each of the content tab, and for the raw links.

FIELD_WORDING is the text displayed for each of the field.

INDEX_WORDING is the text displayed on the different filters of the search page.

CALL_WORDING is the mapping of "call" in the "trace" section of the report to symbols, these are HTML codes but it can also contains every possible textual characters or a combination of both text and HTML codes.

The others variables in this file are dynamic and must not be changed.

### How to set up email addresses to send notifications ?

The configuration of the email addresses in made in the file `conf/email.ini`. When a bug is commented out by a user, a message will automatically be sent to the email address specified in that file. The file consist of a default email address and it's possible to add new addresses for each bugs following the format :
```
bugId=emailAddresses
```
Such as :
```
[bug notification email mapping]
default=student@itu.dk
d549f55=contact@softwareandsystemsgroup.com
60e233a=developer@ituresearch.com
```
Make sure not to remove the default address.

### How to review/delete a comment ?

Go to `data/comments.json`, find the comment to delete and just remove the entry from the file. Just make sure that the JSON file is still formated correctly after modification.
You might want to use a tool to process the file into a readable format for humans such as [http://jsonformatter.curiousconcept.com/](http://jsonformatter.curiousconcept.com/), this tool is also useful to validate the JSON formatting after modification.

### How to modify the routes ?

The mapping of those routes to functions is done in the file `assets/js/app.js` at line 27.

```
routes : {
        "" : "displayHome",
        "home" : "displayHome",
        "home/" : "displayHome",
        "bug" : "displaySearch",
        "bug/" : "displaySearch",
        "bug/:id" : "displayLinuxBug",
        "bug/:repo/:id" : "displayBug",
        "search" : "displaySearch",
        "search/" : "displaySearch",
        "search/:criteria" : "displaySearch",
        "contribution" : "displayContribution",
        "contribution/" : "displayContribution"
    },
```

### How to add a new criteria for indexing the search results ?

To add a new criteria based on one of the key in the YAML file, the only thing to do is to add the new key with its wording into the object INDEX_WORDING in the file `assets/js/model/Store.js`, then the program will automatically create the new tab and order the bugs accordingly.
For example, to add the location as a new criteria we just had a new property "loc" as it is written in the YAML file such as :

```
this.INDEX_WORDING = {
            "type" : "Bug type",
            "fix-in" : "Fix-in",
            "config" : "Configuration",
            "deg" : "Degree",
            "loc" : "Location"
    };
```
It is important to keep in mind that only the bugs who have the property will appear on the search page, for example "C-features" is not on all the bug reports so the list will be smaller for a "C-features" index.

Moreover, regarding the example with "C-features", we can see that we need to specify a separator to cut the different features. In fact some bugs have "FunctionPointers" as the features value but some have "FunctionPointers,PointerAliasing,Structs" so we need to specified comma as the separator. To do so, open the file `assets/js/controller/BugController.js` at line 159 inside the function loadCategories. We can then just add "C-features" in the switch statement, below we can see that a comma separator is already used by "fix-in".

```
    switch(key)
    {
        case "fix-in":
        case "C-features":
            sep = ",";
            break;
        case "config":
            //... some code
            break;
        default:
            sep = " ";
        break;
    } 
```
The reason why separators are not part of the config file `assets/js/model/Store.js` is because some data need more advanced cutting techniques such as "config" which need regular expressions.

For other custom criterias that are not part of the YAML file, such as the "deg" we already have, it is needed to add a new key with the corresponding wording as before and then add new code into the BugController and the BugDisplayer. The easy way is to just search for "deg" in the source code and follow the same approach.

### How to remove data from the table ?

If we want for example to remove the "config" from the table but we still want it on the page, we can edit TABLE_INFO at `assets/js/model/Store.js` and just remove "config".

```
this.TABLE_INFO = ["type", "symptom", "keywords", "config", "source", "fix-in", "C-features", "loc"]; 
```

The information will then appear below the description.

### How to prevent data from being displayed ?

For example, to prevent the data "config" from being displayed, just add "config" to the array HIDDEN_FIELDS in `assets/js/model/Store.js`.

```
this.HIDDEN_FIELDS = ["repo", "source", "config"];
```

### How to add a new tab to the bug page ?

The only things to do to add a new tab is to inject `<li><a href="tab3" role="tab" data-toggle="tab">new tab</a></li>` to the element `#report .nav`
and `<div class="tab-pane" id="tab3">content of the new tab</div>` to the element `#report .tab-content`

Those are references to the DOM elements in `assets/template/BugView.html`.
```
<section id="report">
    <ul class="nav nav-tabs" role="tablist">
    </ul>
    <div class="tab-content">
    </div>
</section>
```

The important thing is that the `href` attribute of the injected `li a` element is the same value as the `id` attribute of the injected `div` element.

We can find an example with dynamic data (for the "trace" tab) in `assets/js/controller/BugDisplayer.js` at line 196 and 197.

```
directory.bugView.$("#report .nav").append('<li><a href="#'+key+'" role="tab" data-toggle="tab">'+getFieldWording(key)+'</a></li>');
directory.bugView.$("#report .tab-content").append('<div class="tab-pane" id="'+key+'">'+label+'</div>');
```

The code tabs for the simplified bug and the simplified patch are using the same principle at line 264 and 265 in `assets/js/controller/BugDisplayer.js`.

```
directory.bugView.$("#report .nav").append('<li><a href="#'+codeId+'" role="tab" data-toggle="tab">'+label+'</a></li>');
directory.bugView.$("#report .tab-content").append(container);
```

##File nomenclature

* assets
    - boostrap : Bootstrap framework files
    - css
        + common.css : the custom css file for the web application, used to define new styling rule or override the ones provided by Bootstrap
    - ico : folder containing the different web app icons
    - images : folder containing interface images
    - js : folder containing all the Javascript programs
        + controller
            * BugController.js : this class is a logical representation of a bug, it handle the data and the behavior of a bug item, it is hence a model-controller in the MVC architecture
            * BugDisplayer.js : this class is controlled by BugController to display data on the screen, it is hence a view-controller in the MVC architecture
        + libs : folder containing third-party Javascript libraries and frameworks
        + model
            * BugLoader.js : class using AJAX to load the bugs data from Bitbucket servers, it extend DataProvider.js
            * DataProvider.js : abstract class used by the loaders
            * Event.js : abstract class used for event-driven calls
            * NewsLoader.js : class using AJAX to load the news from the server, it extend DataProvider.js
            * Store.js : the main model of the web-app, containing all the constants and dynamic shared data of the application
        + utils
            * TwoDimensionsDictionary.js : custom data structure to simplify algorithms and improve execution time
            * Utils : class with utility functions
        + view
            * AView.js : abstract class to encapsulate redundant view behaviors
            * BugView.js : the view of the bug details page, it extend AView.js
            * ContributionView.js : the view of the contribution page, it extend AView.js
            * HomeView.js : the view of the home page, it extend AView.js
            * SearchView.js : the view of the search page, it extend AView.js
        + app.js : entry point of the application when index.html is loaded by the client, also contains the router and the root of the web-app
    - template
        + BugView.html : the template for the single bug details page
        + ContributionView.html : the template for the contribution page with a static content
        + HomeView.html : the template for the home page in which the articles are loaded dynamically
        + SearchView.html : the template for the search page

* conf : folder with the configuration file for email and captcha
    - challenge.math : the file containing the math challenge in Tex format
    - email.ini : the configuration file to map bug Ids to emails addresses for notification sending

* data : folder with all the static content data
    - news : the news articles to be displayed on the home page
        + 01.html
        + 02.html
        + ...
        + 06.html
    - comments.json : the file in which new comments are written

* script
    - class : PHP class folder
        + MyCaptcha.php : class in charge of custom captcha generation and verification
    - captcha.php : the PHP web services returning a challenge to the client
    - comments.php : the PHP web services responsible to check the captcha, save a new comment and send a notification email
    - news.php : the PHP web services to send the news to the client with a pagination system

* index.html : the only HTML page of the web app, loaded by the client as the application entry point

##Authors

IT University of Copenhagen
http://itu.dk/
    
Software and Systems research group
https://sss.wikit.itu.dk/

author Tony Beltramelli
http://www.tonybeltramelli.com
