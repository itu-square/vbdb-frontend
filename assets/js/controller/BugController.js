directory.BugController = function(id, pos, repo)
{
	this.id = directory.utils.getBugUniqueId(repo, id);
	this.repoName = repo;
	this.position = pos;
	this.title;
	this.data = {};
	this.rawLinks = [];
	this.reportJSON;
	this.comments;
	this.displayer = new directory.BugDisplayer(this);
	this.categories;
	this.isActive = false;
	this.config;
	this.bugDegree = 0;
	
	var self = this;
	var counter = 0;
	
	//@public
	
	this.load = function(root, urls)
	{
		$.each(urls, function(i, url){
			path = root.replace(directory.store.PATH, url);
			type = url.substring(0, url.indexOf("/"));
			
			loadContent(path, type, urls.length);
			self.rawLinks[i] = [directory.store.CONTENT_WORDING[type], directory.store.getPath(self.repoName, directory.store.RAW_LINK).replace(directory.store.PATH, url)];
		});
	};
	
	this.display = function()
	{
		$.each(directory.store.bugs, function(i, repo) {
			$.each(repo, function(j, bug) {
				bug.isActive = false;
			});
		});
		
		this.isActive = true;
		
		this.displayer.display();
	};
	
	this.setComments = function(comments)
	{
		this.comments = comments;
		
		if(this.isActive)
		{
			this.displayer.display();
		}
	};
	
	this.match = function(value)
	{
		//function used to search through the bugs
		
		value = value.toLowerCase();
		
		if(this.id != undefined && this.id.toLowerCase().indexOf(value) != -1)
		{
			return true;
		}
		
		for(var type in this.data)
		{
			if(this.data[type] != undefined && this.data[type].toLowerCase().indexOf(value) != -1) return true;
		}
		
		return false;
	};
	
	this.getCommitLink = function()
	{
		return directory.store.TREE[this.repoName]["commit_link"];
	};
	
	this.getCateeeLink = function()
	{
		return directory.store.TREE[this.repoName]["cateee_link"];
	};
	
	this.hide = function()
	{
		this.displayer.hide();
	};
	
	this.show = function()
	{
		this.displayer.show();
	};
	
	//@private
	
	var loadContent = function(path, type, total)
	{
		$.ajax({
			url : path,
			type : "GET",
			dataType : "jsonp",
			success : function(data)
			{
				switch(type)
				{
					case "report":
						self.data["report"] = data.data.replace("`", "'");
						
						//fix YAML tag name formatting
					
						var reg = new RegExp("((!!)[A-Za-z]+[|]+)");
						
						while((result = reg.exec(self.data["report"])) !== null)
						{
							var match = result[0];
							self.data["report"] = self.data["report"].replace(match, match.replace("|", " |"));
						}
						
						self.reportJSON = jsyaml.load(self.data["report"]);
						break;
					default:
						self.data[type] = directory.utils.getEncodedCCode(data.data);
						break;
				}
				
				counter ++;
				
				if(counter == total)
				{
					self.title = directory.utils.getSentenceUppercase(self.reportJSON["descr"].split('\n')[0]);
					
					self.categories = new TwoDimensionsDictionary();
					loadCategories(self.reportJSON);
					
					$(self).trigger(self.isLoadedEvent + self.id, self.position);
				}
			},
			error : function(data)
			{
			}
		});
	};
	
	//definition of the categories for filtering bugs in the search page
	
	var loadCategories = function(json)
	{
		for(var key in json)
		{
			if(typeof json[key] == "string")
			{
				if(directory.store.INDEX_WORDING.hasOwnProperty(key))
				{					
					var value = json[key].replace(/\s+/g, "");
					var sep;
					var matches;
						
					switch(key)
					{
						case "fix-in":
						case "C-features":
							sep = ",";
							break;
						case "config":
							value = value.replace("(", "").replace(")", "");
							sep = /[&|]+/;
							matches = value.split(sep);
							
							self.config = matches;
							self.bugDegree = matches.length;
							
							if(directory.store.INDEX_WORDING.hasOwnProperty("deg"))
							{
								directory.store.categories.add("deg", directory.utils.getOrdinal(self.bugDegree));
								self.categories.add("deg", directory.utils.getOrdinal(self.bugDegree));
							}
							
							break;
						default:
							sep = " ";
						break;
					}
						
					matches = value.split(sep);
					
					$.each(matches, function(i, match) {
						if(match != "")
						{
							directory.store.categories.add(key, match);
							self.categories.add(key, match);
						}
					});
				}
			}else{
				loadCategories(json[key]);
			}
		}
	};
};

directory.BugController.prototype = new directory.Event();
directory.BugController.prototype.constructor = directory.BugController;