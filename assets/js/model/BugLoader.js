directory.BugLoader = function(repoName)
{
	this.isComplete = false;
	
	var self = this;
	var totalNumber = 0;
	
	//@public
	
	this.load = function(bugId)
	{
		if(self.isComplete)
		{
			$(self).trigger(self.isLoadedEvent);
			return;
		}
		
		//load taxonomy file
		
		if(directory.store.taxonomy == null)
		{
			$.ajax({
				url : directory.store.getTaxonomyPath(repoName),
				type : "GET",
				dataType : "jsonp",
				success : function(data)
				{
					directory.store.taxonomy = jsyaml.load(data.data);
				},
				error : function(data)
				{
					console.log(data);
				}
			});
		}
		
		var repoFolders = directory.store.TREE[repoName]["folders"];
		var dictionary = new TwoDimensionsDictionary();
		var counter = 0;
		directory.store.bugs[repoName] = [];
		
		//load the files list of each of the folders
		
		$.each(repoFolders, function(i, folder) {
			$.ajax({
				url : directory.store.getPath(repoName, directory.store.RESOURCE_PATH).replace(directory.store.PATH, folder),
				type : "GET",
				dataType : "jsonp",
				success : function(data) {
					$.each(data.files, function(j, file) {
						var filePath = file.path.substring(directory.store.TREE[repoName]["root"].length + 1, file.path.length);
						dictionary.add(directory.utils.getFileName(filePath), filePath);
					});
					
					counter ++;
					
					if (counter == repoFolders.length)
					{	
						counter = 0;
						var lastEvent = "";
						
						//create bug objects to load their own file data
						
						$.each(dictionary.keys, function(j, key) {
							if(bugId != undefined)
							{
								if(bugId != key) return true;
							}
							
							var bug = new directory.BugController(key, directory.store.bugs[repoName].length, repoName);
							
							directory.store.bugs[repoName].push(bug);
							
							$(bug).bind(self.isLoadedEvent + bug.id, function(event, pos) {
								event.stopPropagation();							
								
								$(directory.store.bugs[repoName][pos]).unbind(self.isLoadedEvent + directory.store.bugs[repoName][pos].id);
								
								if(lastEvent != event.type + event.timeStamp)
								{
									lastEvent = event.type + event.timeStamp;
									counter ++;
									directory.store.globalCounter ++;
								}
								
								if(bugId == undefined) self.updateProgressBar(directory.store.globalCounter, getTotalNumber());
								
								if(counter == directory.store.bugs[repoName].length)
								{
									if(bugId == undefined) self.isComplete = true;
									
									directory.store.repoCounter ++;
									
									if(directory.store.repoCounter == directory.store.bugLoaders.length)
									{
										$(self).trigger(self.isLoadedEvent);
									}
								}
							});
							
							bug.load(directory.store.getPath(repoName, directory.store.RESOURCE_PATH), dictionary.get(key));
						});
						
						self.loadComments();
					}
				},
				error : function(data) {
					console.log(data);
				}
			});
		});
	};
	
	this.loadComments = function()
	{		
		//load all the comments for all the bugs at once
		
		$.ajax({
			url : directory.utils.getNoCachedResourceUrl(directory.store.COMMENTS),
			type : "GET",
			dataType : "json",
			success : function(data) {
				$.each(data.bugs, function(i, bug) {
					$.each(directory.store.bugs, function(j, repo) {
						$.each(repo, function(k, b) {
							if (bug.id == b.id) {
								b.setComments(bug.comments);
							}
						});
					});
				});
			}
		});
	};
	
	//@private

	var getTotalNumber = function()
	{
		if(totalNumber != 0)
		{
			return totalNumber;
		}
		
		$.each(directory.store.bugs, function(i, repo) {
			$.each(repo, function(j, bug) {
				totalNumber ++;
			});
		});
		
		return totalNumber;
	};
};

directory.BugLoader.prototype = new directory.DataProvider();
directory.BugLoader.prototype.constructor = directory.BugLoader;