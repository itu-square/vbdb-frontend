directory.DataProvider = function()
{	
	this.load = function(toUpdate) {
		console.log("override the load method");
	};
	
	this.updateProgressBar = function(current, total)
	{
		percent = Math.round((current * 100) / total);
		percent = percent <= 100 ? percent : 100;
		
		$("#bar .progress-bar").text("Loading in progress " + percent + "%");
		$("#bar .progress-bar").attr("aria-valuenow", percent);
		$("#bar .progress-bar").css("width", percent+"%");
		
		if(percent >= 100)
		{
			$("#bar").delay(900).fadeOut();
		}
		
		if($("#bar").css("display") == "none")
		{
			$("#bar").fadeIn();
		}
	};
};

directory.DataProvider.prototype = new directory.Event();
directory.DataProvider.prototype.constructor = directory.DataProvider;