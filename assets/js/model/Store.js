directory.Store = function()
{
	this.TREE = {
			"Linux" : {
				"repository" : "modelsteam/vbdb",
				"root" : "linux",
				"branch" : "default",
				"logo" : "http://upload.wikimedia.org/wikipedia/commons/a/af/Tux.png",
 				"folders" : ["report", "simple", "patch", "intra"],
				"commit_link" : "http://git.kernel.org/cgit/linux/kernel/git/stable/linux-stable.git/commit/?id=",
				"cateee_link": "http://cateee.net/lkddb/web-lkddb/"
			},
			"BusyBox" : {
				"repository" : "modelsteam/vbdb",
				"root" : "busybox",
				"branch" : "default",
				"logo" : "http://www.stlinux.com/sites/default/files/busybox1.png",
				"folders" : ["report", "simple", "patch", "intra"],
				"commit_link" : "http://git.busybox.net/busybox/commit/?id="
			},
			"Marlin" : {
				"repository" : "modelsteam/vbdb",
				"root" : "marlin",
				"branch" : "default",
				"logo" : "https://github.com/MarlinFirmware/Marlin/blob/Release/Documentation/Logo/Marlin%20Logo%20GitHub.png",
				"folders" : ["report", "simple", "patch", "intra"],
				"commit_link" : "https://github.com/MarlinFirmware/Marlin/commit/"
			}
	};

	//constants
	this.TABLE_INFO = ["credits", "type", "symptom", "keywords", "config", "source", "fix-in", "C-features", "loc"];
	this.HIDDEN_FIELDS = ["repo", "source"];

	this.PATH = "{path}";
	this.BRANCH = "{branch}";
	this.REPOSITORY = "{repository}";

	this.RESOURCE_PATH = "https://bitbucket.org/api/1.0/repositories/"+this.REPOSITORY+"/src/"+this.BRANCH+"/"+this.PATH;
	this.RAW_LINK = "https://bitbucket.org/"+this.REPOSITORY+"/raw/"+this.BRANCH+"/"+this.PATH;
	this.CWE_LINK = "http://cwe.mitre.org/data/definitions/"+this.PATH+".html";
	this.TAXONOMY_PATH = "taxonomy";

	this.COMMENTS = "data/comments.json";
	this.COMMENTS_WEBSERVICE = "script/comments.php";

	this.NEWS = "data/news/";
	this.NEWS_WEBSERVICE = "script/news.php";

	this.CAPTCHA_WEBSERVICE = "script/captcha.php";

	this.DISCUSSION_TAB_LABEL = "Discussion";
	this.RESULT_COUNTER_LABEL = "result{s} retrieved";

	//for loading time estimation
	this.globalCounter = 0;
	this.repoCounter = 0;

	//arrays
	this.news = [];
	this.bugLoaders = [];

	//dictionary
	this.categories = new TwoDimensionsDictionary();

	//objects
	this.bugs = {};
	this.CONTENT_WORDING = {
			"report" : "Info",
			"simple" : "Simplified bug",
			"patch" : "Simplified patch",
			"intra" : "Single function bug",
	};
	this.FIELD_WORDING = {
			"descr" : "Description",
			"loc" : "Location"
	};
	this.INDEX_WORDING = {
			"type" : "Bug type",
			"fix-in" : "Fix-in",
			"config" : "Configuration",
			"deg" : "Degree",
			"C-features" : "C-features"
	};
	this.CALL_WORDING = {
			"dyn-call" : "&#8627;",
			"call" : "&#8618;"
	};
	this.taxonomy = null;

	for(var repoName in this.TREE) {
		//create one bug loader per repository

		this.bugLoaders.push(new directory.BugLoader(repoName));
		this.bugs[repoName] = [];
	}

	this.newsLoader = new directory.NewsLoader();

	this.getPath = function(repoName, path)
	{
		var repoPath = this.TREE[repoName]["repository"];
		var repoBranch = this.TREE[repoName]["branch"];
		var repoRoot = this.TREE[repoName]["root"];

		path = path.replace(this.REPOSITORY, repoPath);
		path = path.replace(this.BRANCH, repoBranch + "/" + repoRoot);

		return path;
	};

	this.getTaxonomyPath = function(repoName, path)
	{
		var repoPath = this.TREE[repoName]["repository"];
		var repoBranch = this.TREE[repoName]["branch"];

		path = this.RESOURCE_PATH;
		path = path.replace(this.REPOSITORY, repoPath);
		path = path.replace(this.BRANCH, repoBranch);
		path = path.replace(this.PATH, this.TAXONOMY_PATH);

		return path;
	};
};
