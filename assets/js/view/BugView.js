directory.BugView = directory.AView.extend({
	
	bugId : null,
	
	init : function() {
		$.SyntaxHighlighter.init();
		this.index = 0;
	},
	
	build : function(id) {
		this.bugId = id;
		
		directory.BugView.__super__.build.apply(this);
	},
	
	load : function() {
		self = this;
		
		//load bugs from all repositories
		$.each(directory.store.bugLoaders, function(i, bugLoader) {
			self.loadData(bugLoader, bugLoader.isLoadedEvent, this.bugId);
		});
	},
	
	loaded : function() {
		this.display(this.bugId);
	},

	buildComments : function() {
		var shown = false;
		
		this.$('#comments_modal').on('hidden.bs.modal', function() {
			
			//call when the comment pop up is closed
			
			if(!shown) return;
			shown = false;
			
			self.$(".modal-body > center").addClass("hide");
			self.$("#challenge").addClass("hide");
			self.$(".modal-footer").addClass("hide");
			self.$("#comments_modal .alert-warning").addClass("hide");
			self.$("#comments_modal .alert-danger").addClass("hide");
			self.$("#comments_modal .alert-success").addClass("hide");
			
			self.$("#comments_modal textarea").val("");
			self.$("#challenge input").val("");
		});
		
		this.$('#comments_modal').on('show.bs.modal', function() {
			
			//call when the comment pop up is opened
			
			if(shown) return;
			shown = true;
			
			var challengeIndex = 0;
			
			$.ajax({
				url : directory.store.CAPTCHA_WEBSERVICE,
				type : "GET",
				dataType : "json",
				success : function(data, event) {
					if(data.hasOwnProperty("challenge"))
					{
						challengeIndex = data.index;

						self.$("#challenge").removeClass("hide");
						self.$(".modal-footer").removeClass("hide");
						self.$(".modal-body > center").addClass("hide");
						
						self.$("#challenge span").text("$$" + data.challenge + "$$");
						MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
					}else{
						self.$(".modal-body > center").addClass("hide");
						self.$("#comments_modal .alert-danger").removeClass("hide");
					}
				}
			});
			
			self.$("#submit").unbind("click");
			self.$("#submit").bind("click", function(event) {
				
				//function called when a new comment is submitted
				
				var id = self.$("#bug_content .page-header > span").text();
				var content = self.$("#comments_modal textarea").val();
				var email = self.$("#email_field").val();
				var answer = self.$("#challenge input").val();
				
				self.$(".modal-body > center").removeClass("hide");
				
				if (id != "" && content != "" && (email == "" || directory.utils.isValidEmail(email)) && (answer != "" && directory.utils.isNumeric(answer)))
				{
					$.ajax({
						url : directory.store.COMMENTS_WEBSERVICE,
						type : "POST",
						dataType : "json",
						data : {
							id : id,
							author : self.$("#name_field").val() != "" ? self.$("#name_field").val() : "anonymous",
							email: email,
							content : content,
							answer : answer,
							index : challengeIndex
						},
						success : function(data) {
							if(data.status == "success")
							{
								$.each(directory.store.bugLoaders, function(i, bugLoader) {
									bugLoader.loadComments();
								});

								self.$("#comments_modal .alert-success").removeClass("hide");
								self.$("#comments_modal .alert-danger").addClass("hide");
								self.$("#comments_modal .alert-warning").addClass("hide");					
							}else{
								self.$("#comments_modal .alert-danger").text(data);

								self.$("#comments_modal .alert-danger").removeClass("hide");
								self.$("#comments_modal .alert-success").addClass("hide");
								self.$("#comments_modal .alert-warning").addClass("hide");
							}
							
							window.setTimeout(function() {
								self.$("#comments_modal").modal("hide");
							}, 900);
							
							self.$(".modal-body > center").addClass("hide");
						},
						error : function(data) {
							if(data.hasOwnProperty("responseText"))
							{
								self.$("#comments_modal .alert-danger").text(data.responseText);
							}

							self.$("#comments_modal .alert-danger").removeClass("hide");
							self.$("#comments_modal .alert-success").addClass("hide");
							self.$("#comments_modal .alert-warning").addClass("hide");
							
							self.$(".modal-body > center").addClass("hide");
						}
					});
				} else {
					self.$("#comments_modal .alert-warning").removeClass("hide");
					self.$("#comments_modal .alert-danger").addClass("hide");
					self.$("#comments_modal .alert-success").addClass("hide");
					self.$(".modal-body > center").addClass("hide");
				}
			});
		});
	},
	
	display : function(id)
	{		
		self = this;
		
		this.bugId = id;
		
		this.buildComments();
		
		if(this.bugId != null && this.bugId != undefined && this.bugId != "")
		{
			var isFound = false;
			
			$.each(directory.store.bugs, function(i, repo) {
				$.each(repo, function(j, bug) {
					if(self.bugId == bug.id)
					{
						bug.display();
						
						isFound = true;
						
						return false;
					}
				});
			});
			
			if(!isFound)
			{
				this.showInfo();
			}	
		}else{
			this.showInfo();
		}
		
		directory.router.navigate("bug/"+this.bugId);
		
		this.toggleNav();
	},
	
	showInfo : function()
	{
		this.$("#bug_content").addClass("hide");
		this.$("#info").removeClass("hide");
	},
	
	hideInfo : function()
	{
		this.$("#info").addClass("hide");
		this.$("#bug_content").removeClass("hide");
	}
}); 