directory.SearchView = directory.AView.extend({
	
	searchCriteria : "",
	displayedCategories : null,
	isReady : false,
	
	init : function() {
		this.index = 2;
	},
	
	build : function(criteria) {
		this.searchCriteria = criteria;
		
		directory.BugView.__super__.build.apply(this);
	},
	
	load : function() {
		self = this;
		
		//load bugs from all repositories
		$.each(directory.store.bugLoaders, function(i, bugLoader) {
			self.loadData(bugLoader, bugLoader.isLoadedEvent);
		});
	},
	
	loaded : function() {
		this.isReady = true;
		this.buildIndex();
	},
	
	display : function(criteria) {
		
		if(directory.store.bugs.length == 0)
		{
			this.build(criteria);
			return;
		}
		
		this.searchCriteria = criteria;
		
		self = this;
		
		this.toggleNav();
		this.buildIndex();
	},
	
	buildIndex : function()
	{
		if(!this.isReady) return;
		
		directory.router.navigate("search/"+this.searchCriteria);
				
		var index = this.$("#index_nav .nav li.active").index();
		index = index == -1 ? 0 : index - 2;
		
		this.$("#index_nav .nav .index a").unbind("click");
		this.$("#index_nav .nav .index > a > .dropdown ul a").unbind("click");
		
		this.$("#index_nav .nav .index").remove();
		this.$("#index_nav .nav .checkbox-filter label").remove();
		
		//show repo filters
		
		for(var repoName in directory.store.TREE) {
			var img = '<img src="'+directory.store.TREE[repoName]["logo"]+'" alt="'+repoName+' logo"/>';
			var checkboxContainer = $('<label class="checkbox-inline"><input type="checkbox" value="'+repoName+'" checked/>'+repoName+img+'</label>');
			
			this.$("#index_nav .nav .checkbox-filter").append(checkboxContainer);
			
			checkboxContainer.find("input").click(function() {
				self.indexBy(self.$("#index_nav .nav li.index.active a").attr("class"));
			});
		}
		
		//construct the filter list
		
		for(var key in directory.store.INDEX_WORDING) {
			var indexButton = $('<li class="index"><a href="#" class="'+key+'">'+directory.store.INDEX_WORDING[key]+'</a></li>');
			
			directory.store.categories.get(key).sort();
			
			if(this.$("#index_nav .nav li.index").length == index)
			{
				indexButton.addClass("active");
				
				this.indexBy(key);
			}
			
			var dropDownMenu = $('<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">&nbsp;<span class="caret"></span></a><ul class="dropdown-menu" role="menu"></ul></li>');
			
			$.each(directory.store.categories.content[key], function(i, category) {
				var catId = self.getCategoryId(category);
				dropDownMenu.find("ul").append('<li><a class="'+catId+'" href="#'+catId+'">'+self.getCategoryLabel(category)+'</a></li>');
			});
			
			indexButton.find("a").append(dropDownMenu);
				
			this.$("#index_nav .nav").append(indexButton);
		}
		
		this.$("#index_nav .nav .index > a").bind("click", function(event) {
			event.preventDefault();
			
			$("#index_nav .nav li").removeClass("active");
				
			self.indexBy($(this).attr("class"));
				
			$(this).parent().addClass("active");
		});
		
		this.$("#index_nav .nav .index > a > .dropdown ul a").bind("click", function(event) {
			event.preventDefault();
			
			directory.router.navigate($(this).attr("href"), true);
			directory.router.navigate("search/"+encodeURIComponent(self.searchCriteria), false);
		});
		
		this.$("#index_nav").removeClass("hide");
	},
	
	refreshCategories : function(category)
	{
		var found = false;
		
		$.each(this.displayedCategories, function(l, cat) {
			if(cat == category)
			{
				found = true;
				return false;
			}								
		});
		
		if(!found) this.displayedCategories.push(category);
	},
	
	refreshCategoryIndex : function()
	{
		this.$("#index_nav .nav .index > a > .dropdown ul a").addClass("hide");
		
		$.each(this.displayedCategories, function(i, category) {
			self.$("#index_nav .nav .index > a > .dropdown ul a."+category).removeClass("hide");
		});
	},
	
	indexBy : function(key)
	{
		this.$("#search_list").empty();
		
		var bugCounter = 0;
		this.displayedCategories = [];
		
		filterStates = this.getRepoFilterStates();
		
		//function called to create section categories and display bugs
		
		$.each(directory.store.categories.content[key], function(i, category) {
			var hasSection = false;
			
			$.each(directory.store.bugs, function(i, repo) {
				$.each(repo, function(j, bug) {
					var hasRepoFilter = false;
					
					//filter bugs by repository
					
					$.each(filterStates, function(k, state) {
						if(bug.repoName == state[0] && state[1] == true)
						{
							hasRepoFilter = true;
						}
					});
					
					//filter bugs by search criteria and category
					
					if(hasRepoFilter && bug.match(self.searchCriteria) && bug.categories.content.hasOwnProperty(key))
					{
						$.each(bug.categories.content[key], function(k, value) {
							if(category == value)
							{
								if(!hasSection)
								{
									hasSection = true;
									
									self.$("#search_list").append('<a id="'+self.getCategoryId(category)+'" class="list-group-item disabled">'+self.getCategoryLabel(category)+'</a>');
								}
								
								bugCounter ++;

								var img = '<img src="'+directory.store.TREE[bug.repoName]["logo"]+'" alt="'+bug.repoName+' logo"/>';
								self.$("#search_list").append('<a href="#bug/'+bug.id+'" class="list-group-item">'+img+bug.title+'</a>');
								
								self.refreshCategories(self.getCategoryId(category));
							}
						});
					}
				});
			});
		});
		
		if(this.$("#search_list a").length == 0)
		{
			this.showInfo();
		}else{
			this.hideInfo();
		}
		
		var counterLabel = bugCounter + " " + directory.store.RESULT_COUNTER_LABEL.replace("{s}", bugCounter > 1 ? "s" : "");
		
		this.$("#index_nav .badge").text(counterLabel);
		this.refreshCategoryIndex();
	},
	
	getRepoFilterStates : function()
	{
		var states = [];
		
		this.$("#index_nav .nav .checkbox-filter label input").each(function(i, label){
	    	states.push([$(this).attr("value"), $(this).is(':checked')]);
	    });
	    
	    return states;
	},
	
	getCategoryLabel : function(category)
	{
		var label = category;
		
		if (directory.store.taxonomy.hasOwnProperty(category))
		{
			var node = directory.store.taxonomy[category];
			label = node.text;	
		}
		
		return label;
	},
	
	getCategoryId : function(category)
	{
		category = category.toLowerCase();
		category = category.replace(new RegExp(" ", "g"), "_");
		category = category.replace(new RegExp("!", "g"), "not");
		
		return category;
	},
	
	showInfo : function()
	{
		$("#info").removeClass("hide");
	},
	
	hideInfo : function()
	{
		$("#info").addClass("hide");
	}
});
