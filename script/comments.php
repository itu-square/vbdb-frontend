<?php

include_once 'class/CustomSimpleCaptcha.php';

$QUESTION_PATH = "../conf/challenge.math";

//check captcha challenge response

$answer = $_POST["answer"];
$index = $_POST["index"];

$captcha = new CustomSimpleCaptcha($QUESTION_PATH);
$result = json_decode($captcha->checkAnswer($index, $answer), true);
		
if ($result["status"] != "success")
{
	echo ("The challenge wasn't answered correctly, please try again.");
	return;
}

$COMMENTS_PATH = "../data/comments.json";
$EMAIL_PATH = "../conf/email.ini";

$id = $_POST["id"];
$author = $_POST["author"];
$email = $_POST["email"];
$content = $_POST["content"];

//add new comment to file

$file = file_get_contents($COMMENTS_PATH);
$all = json_decode($file, true);

if ($id == "" || $author == "" || $content == "") {
	echo "Please fill the fields correcly.";
	return;
}

$date = new DateTime();

$comment = array('author' => $author, 'email' => $email, 'date' => $date->format('d/m/Y H:i:s'), 'content' => htmlentities($content));

$i = 0;
$l = count($all["bugs"]);
$found = false;

for ($i; $i < $l; $i++) {
	$item = $all["bugs"][$i];

	if ($item["id"] == $id) {
		array_push($item["comments"], $comment);
		$all["bugs"][$i] = $item;
		$found = true;
		break;
	}
}

if($found == false)
{
	$bug = array('id' => $id, 'comments' => array($comment));
	array_push($all["bugs"], $bug);
}

$fw = fopen($COMMENTS_PATH, 'w');
fwrite($fw, json_encode($all));
fclose($fw);

echo json_encode(array("status" => "success"));

//send notification email

$emails = parse_ini_file($EMAIL_PATH);
$emailTo = $emails["default"];

if(array_key_exists($id, $emails))
{
	$emailTo = $emails[$id];
}

$header = 'New comment on bug '.$id;
$body = "From ".$author."<".$email."> at ".$date->format('d/m/Y H:i:s')."\n\n".$content."\n\nhttp://130.226.142.211/#bug/".$id;
    	
mail($emailTo, $header, $body);

?>