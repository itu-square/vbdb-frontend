<?php

$page = $_GET["page"];

$NEWS_PATH = "../data/news/";
$NEWS_NUMBER = 2;

if(!is_numeric($page)) return;

$files = array();

if(is_dir($NEWS_PATH))
{
	if($dir = opendir($NEWS_PATH))
	{
		while(($file = readdir($dir)) != false)
		{
			if(strrpos($file, ".html"))
			{
				$files[] = $file;
			}
		}
		
		closedir($dir);
	}
	
	rsort($files);

	$pageNumber = ceil(count($files) / $NEWS_NUMBER);
	
	if($page >= $pageNumber) return;
	
	$i = $page * $NEWS_NUMBER;
	$l = $i + $NEWS_NUMBER;
	$l = $l > count($files) ? count($files) : $l;
	
	$result = array();

	for ($i; $i < $l; $i++) {
		$result[] = $files[$i];
	}
	
	$next = $page + 1;
	$next = $next == $pageNumber ? "null" : $next;
	
	$json = array('news' => $result, 'next' => $next);
	
	echo json_encode($json);
}

?>
